import java.io.IOException;
import java.util.Scanner;

import com.lloseng.ocsf.client.AbstractClient;


public class ChatClient extends AbstractClient{

	public ChatClient(String host, int port) {
		super(host, port);
	}

	protected void handleMessageFromServer(Object msg) {
		System.out.println("> " + msg);
	}
	
	public static void main(String[]args){
//		String host = "158.108.231.12";
		String host = "158.128.181.241";
		int port = 5001;
		
		ChatClient cc = new ChatClient(host,port);

		while(true){
			
			try {
				cc.openConnection();
				cc.sendToServer("p");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
