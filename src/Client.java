import java.io.IOException;
import java.util.Scanner;

import com.lloseng.ocsf.client.AbstractClient;


public class Client extends AbstractClient{

	public Client(String host, int port) {
		super(host, port);
	}

	protected void handleMessageFromServer(Object msg) {
		System.out.println("> " + msg);
	}
	
	public static void main(String[]args){
//		String host = "158.108.231.12";
		String host = "158.108.181.241";
		int port = 5001;
		
		ChatClient cc = new ChatClient(host,port);
		String in;
		Scanner input = new Scanner(System.in);
		while(true){
			
			try {
				cc.openConnection();
				in = input.nextLine();
				if(in.equals("quit")){
					cc.closeConnection();
					System.out.println("Disconnected");
					break;
				}
				else cc.sendToServer(in);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
