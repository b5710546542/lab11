import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;


public class EchoServer extends AbstractServer{

	private final static int port = 5555;
	private final static int LOGIN = 1;
	private final static int LOGOUT = 2;
	private final static String HOST = "158.108.233.147";
	private List<ConnectionToClient> clients = new ArrayList<ConnectionToClient>();
	private Map<String,String>map = new HashMap<String,String>();
	
	public EchoServer(int port) {
		super(port);
	}

	@Override
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		if(!(msg instanceof String)){
//			String message = (String)msg;
		}
		
		String message = (String)msg;
		int state = (Integer)client.getInfo("state");
		String username = "";
		String toSomeone = "";
		username = (String)client.getInfo("username");
		
		switch(state){
		case LOGOUT:
			if(message.matches("Login \\w+")){
				username = message.substring(6).trim();
				client.setInfo("username", username);
				client.setInfo("state" , LOGIN);
				map.put(username, "");
				System.out.println(username + "has logged in");
				super.sendToAllClients(username + "has logged in");
			}
			else sendToClient(client, "Plese log in");
			break;
		
		case LOGIN:
			username = (String)client.getInfo("username");
			if(message.equalsIgnoreCase("Logout")){
				super.sendToAllClients(username + "Logged out");
				System.out.println(username + "Logged out");
				client.setInfo("state", LOGOUT);
				map.remove(username);
			}
			else if(message.matches("to \\w+")){
				if(map.containsKey(message.substring(2).trim())){
					System.out.println(username + "connect to" + message.substring(2).trim());
					map.replace(username, message.substring(2).trim());	
				}
			}
			else if(message.length() > 0){
				username = (String)client.getInfo("username");
				if(!map.get(username).equals("")){
					Thread[] thr = this.getClientConnections();
					ConnectionToClient[]  c = new ConnectionToClient[thr.length];
					for(int i = 0; i < c.length ; i++){
						c[i] = (ConnectionToClient)thr[i];
					}

//					ConnectionToClient[] c = (ConnectionToClient[])this.getClientConnections();
					for(int i = 0 ; i < c.length ; i++){
						if(c[i].getInfo("username").toString().equals(map.get(username))){
							try {
								c[i].sendToClient(message);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					
				}
				else
					super.sendToAllClients(username + " : " + message);
			}
			break;
			
		}

	}
	
	public void sendToClient(ConnectionToClient client , String str){
		try{
			client.sendToClient(str);
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
//	public static void main(String[]args){
//		
//		EchoServer server = new EchoServer(port);
//		Scanner input = new Scanner(System.in);
//
//		try {
//			server.listen();
//			
//		} catch (IOException e) {
//			System.out.println("Couldn't start server:");
//			System.out.println(e);
//		}
//	}
	
	protected void clientConnected(ConnectionToClient client){
		super.sendToAllClients("You are connect to " + HOST );
		client.setInfo("state", LOGOUT);
		clients.add(client);
	}
	
	protected synchronized void clientDisconnected(ConnectionToClient client){
//		super.sendToAllClients("You are connect to " + HOST );
//		client.setInfo("state", LOGOUT);
//		clients.add(client);
	}
	
	public static void main(String[]args){
		EchoServer server = new EchoServer(port);
		Scanner input = new Scanner(System.in);

		try {
			server.listen();
			
		} catch (IOException e) {
			System.out.println("Couldn't start server:");
			System.out.println(e);
		}
	}

}
